import java.util.Random;
import java.util.Scanner;

public class Rumors {

    public static void main(String[] args) {
        System.out.println("Input please count of friends on party from 2 ");
        int numberOfFriends = readFromConsole();
        estimateProbability(numberOfFriends);
        expectedNumberOfFriends(numberOfFriends, 100);
    }

    public static boolean checkAllFriendsHear(boolean[] friends) {
        boolean condition = false;
        for (int i = 0; i < friends.length; i++) {
            if (friends[i] != true)
                condition = true;
        }
        return condition;
    }

    public static void expectedNumberOfFriends(int numberOfFriends, int countOfCycle) {
        int countOfPeopels = 0;
        for (int i = 0; i < countOfCycle; i++) {
            boolean[] friends = createFriends(numberOfFriends);
            int toldRumor = 1;
            countOfPeopels+=2;
            while (checkAllFriendsHear(friends)) {
                int listenRumor = randomizeValue(friends.length, toldRumor);
                if (friends[listenRumor]) {
                    friends = createFriends(numberOfFriends);
                    toldRumor = 1;
                    break;
                } else {
                    toldRumor = listenRumor;
                    friends[listenRumor] = true;
                    countOfPeopels++;
                }
            }
        }
        System.out.println("\nThe middle count of people to hear the rumor for a " + countOfCycle + " loops is "+ Math.round((double)countOfPeopels/countOfCycle) + " people");
    }

    public static void estimateProbability(int numberOfFriends) {
        boolean[] friends = createFriends(numberOfFriends);
        int cycle = 1;
        int toldRumor = 1;
        while (checkAllFriendsHear(friends)) {
            int listenRumor = randomizeValue(friends.length, toldRumor);
            if (friends[listenRumor]) {
                cycle++;
                friends = createFriends(numberOfFriends);
                toldRumor = 1;
            } else {
                toldRumor = listenRumor;
                friends[listenRumor] = true;
            }
        }
        System.out.printf("Probability all friends hear rumors = %f", (double) 1 / cycle);
    }

    public static int randomizeValue(int numberOfFriends, int currentFriend) {
        Random random = new Random();
        int randomValue = random.nextInt((numberOfFriends - 1) + 1);
        if (randomValue != currentFriend)
            return randomValue;
        else
            return randomizeValue(numberOfFriends, currentFriend);
    }

    public static int readFromConsole() {
        Scanner scan = new Scanner(System.in);
        int countOfFriends = scan.nextInt();
        scan.close();
        if (countOfFriends > 2)
            return countOfFriends;
        else {
            System.out.println("Count of friends should be more than 2!");
            return readFromConsole();
        }

    }

    public static boolean[] createFriends(int numberOfFriends) {
        boolean[] friends = new boolean[numberOfFriends];
        friends[0] = true;
        friends[1] = true;
        return friends;
    }

}
